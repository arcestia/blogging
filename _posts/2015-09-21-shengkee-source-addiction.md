---
layout: post
title:  "Shengkee, the Source of My Addiction"
description: An explanation as to why I am addicted to Milk Tea. 
category: boba, story
tags: [ milktea, story ]
image: /assets/media/addictions/bobacups.jpg
comments: true
---

Many people who know me now know that I have an addiction to milk tea. My addiciton might just be worse than those who requires a cup of coffee in the morning to start working. They know this fact because I have gotten a peak of five cups a day within the past year, and a low of one. Though if I am honest with myself, ever since the semester has started, I've averaged around two cups a day. While this amount is about how much a regular coffee addict drinks, the cost is insane. 

<center><img src="{{page.image}}" width="70%" /></center>

But it wasn't always like this... I actually rarely ever got milk tea when I was in high school. I didn't even have my first sip of milk tea until my *sophomore year of high school* while majority of my peers are already familiar with the taste of the drink and the texture of the tapioca balls. 

In high school, milk tea was a rare occasion. This is mostly because there weren't many milk tea shops around the area where I lived and went to school. If my friends and I wanted to get milk tea, we would have to travel for about fifteen to twenty minutes by foot, up and down San Francisco's hills -- and all of this trouble for milk tea that I now consider to be "low tier." If we wanted better drinks, the travel time would have been forty five to an hour by bus. 

So obviously, milk tea was out of the picture for most of our after school activities. Instead, we opt to "study" for our classes and "work" on our homework at a nearby coffee shop called [Mama's Art Café](http://mamasf.com/) instead. And that was where I spent most of my time and money in high school. Not on milk tea but on an ice Chai Latte (often with whip cream on top), with a vegetarian bagel on the side. I was a hipster before hipster was a thing. 

But then all good things have to come to an end, and I graduated from high school. That means I have to find another place to fulfill the emptiness in my day once I arrived to UC Berkeley. Which, by the way, was actually the summer before my freshman year because I had to take mandatory summer classes. Like actually mandatory because those who accepted me wanted to make sure I didn't totally fail out of UC Berkeley. So here's the solution they gave me: take summer school classes, and if I failed, I can't attend UC Berkeley.

Uh OK. Pretty much I risked going to college (in general) for UC Berkeley because I wouldn't have SIR'd to other schools. All is good though -- I ended up passing with flying colors. 

And this is where the addiction started....

###Berkeley's summer is blazing hot.

It's actually a lot hotter than I anticipated it to be. So to cool down, here are the options I am given: Peet's Coffee, Starbucks, ShengKee Bakery's milk tea, and a smoothie at the Golden Bear Café. While every single option is great, a smoothie is too cold and icy for me, along with a frappuccino and javiva. 

And in all honesty, all of the previously mentioned options are too sweet for me and ended up crashing me more than it helped me stay awake -- which was something I desperately needed over the summer. I mean, after all, I needed to pass all of these classes. So I begin my search for the perfect drink to keep me alive while I type out paragraphs after paragraphs of analysis on Chicano Studies.

Within the first week, my friend (let's call him Ryan) and I decided to go to [ShengKee Bakery](http://www.shengkee.com/en/index.aspx) because Ryan was persistent on buying some Asian pastries. So yeah, we did not initially start going there for the milk tea but rather for pastries that reminded us of home. 

And while we were at it, we spied the menu which sold a **chubby** cup of milk tea for $2.99 -- and toppings are free!! For two cheap, Asian, and college students, this was a *great* deal. As a result, Ryan and I picked our drinks. Tiger Milk Tea with boba for Ryan, and Jasmine Milk Tea with boba for me. I had the Jasmine Milk Tea specially made because at the time, they did not have it. Instead they had Jasmine Tea, and ended up mixing it with the regular half and half. 

The first sip was *amazing*. I don't think I can describe you how all of my senses felt when I took the first sip of the iced cold tea mixed with half and half, boba balls, and sugar. I chugged the first cup I got. I don't think (up to that point) that I have ever tasted soft and chewy boba balls, and a Jasmine Milk Tea that was in such a perfect consistency. 

Ryan and I enjoyed our diabetes sized drink as we walked back to our dorm to continue our readings on Chicano Studies. And for once, I did not feel the need to take a quick nap to keep myself awake -- the milk tea had enough caffeine to keep me cracked out to go through over 50 pages text! 

Eventually, we just ended up buying a cup whenever we felt as though our energy was low. The trip down to Shengkee and back up to our dorm served initially as a break. A break with a sweet taste. Of course we both didn't know how big of a problem this will spiral into. 

By the third week of the program, two week after we discovered this amazing crack called ShengKee Bakery's milk tea, we were known as the regulars there by an amazing cashier named Marie. Every time we walked into the store while she's working, she already knew what we were going to buy. A Tiger Milk Tea with boba for Ryan, and a Jasmine Milk Tea with boba for me! Marie often teases us about our consistency and our loyalty to ShengKee Bakery. And of course, she made a comment about how we were addicted to our drinks. 

We didn't admit to it at the time. 

Then came the deal the turned what seemed to be a harmless routine into an addiction I can't escape:

####Buy 2 Chubby Cups for $4.99

<br>
Oh god, I can't even tell you how excited Ryan and I were when we saw this deal. Now if one of us wanted milk tea, *both of us has to get it*. Essentially we were each other's enablers. At this point, because the deal was provoking us, pressuring us to make a good use out of it, we kept buying the drinks to get the deal. 

Now, for weeks and weeks, we repeatedly run to Shengkee in the afternoon (as they don't serve milk tea until 10am -- by then we were already in classes until the afternoon) to buy our daily cup of milk tea. This deal lasted through the entire summer. Honestly, we thought we would have gotten sick of milk tea after a week or so of daily intake. This wasn't the case... instead, as midterm (and essay writing) season rolled around, we started staying up until 3am or 4am. 

Guess what we used instead of coffee to stay awake? *Milk tea.* As a reward for ourselves, and a source of caffeine to keep ourselves awake, before Shengkee Bakery closed (11pm on weekdays, and 12am on weekends), we ran to purchase our drinks. Before we know it, we got two milk teas a day. These cups were 25oz each. 

Drinking milk tea became a very consistent part of my life. I needed it -- I spent too much time with it and I can no longer escape from it. By training myself to become more effective with the addictive drink by my side, I can't focus and work without it. It becomes a need to be productive. 

*Thanks, Ryan, you are an enabler who got me addicted to milk tea. And you're welcome for the addiction.*

Thank you [Cathleen Jia](flickr.com/photos/cathleenjia/) for providing the photo!
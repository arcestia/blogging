---
layout: post
title:  This Blog & Me
description: An introduction to my blog, and what direction I'm taking my blog.
category: site-management
tags: [ updates ]
image: /assets/media/this_blog_and_me/img.png
comments: true
---

Welcome to my [dark playground](http://waitbutwhy.com/2013/10/why-procrastinators-procrastinate.html)! I will be spending my "*spare*" time writing up posts that appear relevant to me and my life in the moment. Notice how I put spare in quotes -- meaning that I will probably be writing these posts when I shouldn't be. Oops. At least I can admit it. 

Don't worry, I'm a *professional* (notice the sarcasm). I have had a long history in relation to blogs, which I will talk about later on in this post. But for now, I want to warn you about my blogging habits: my goal is to post **an average of twice a month**. You may see an influx of posts in the beginning of the school year, and see me die down as the academic semester because I will be swamped with work and procrastination. 

I apologize in advance, this will be one of my longer posts :)

<center><img src="{{page.image}}" width="70%" /></center>

I figure if you are reading this post, you are curious as to my goals, intentions, and topic for this blog. Unless I never update my blog and this remains as my only post. (I really hope that doesn't happen.) So to make it easier on you, I broke down this topic into sections so it can be easily navigated. This post is primarily about my blog, and less about me. [Click here if you want to read more about me!](/about/)

###My Blogging History

This is not my first time blogging -- maybe my first time blogging "professionally" (whatever that means) -- but in the past, I have kept a personal blog where I would post *religiously*. I'm talking about an average of **1.25 posts a day**, focusing on daily reflections. However this only lasted for about a year (my Freshman and beginning of Sophomore year of high school). I stopped completely by the end of my Junior year.

Owning a blog and a website have been my goal for quite a long time. "A long time" is an understatement. I wanted to have my own blog and site way before 2009 (I have concrete proof in 2009 through my ridiculous Facebook post). But six/seven years later, I am able to fulfill my goal in having my own website with a blog hosted on it. I'm quite proud.

###My Goals for My Blog
I want this blog to hold some resemblance to the blogs I've always wanted to have when I was younger. Including having some similarities to my personal blog I had back in high school. This means that at the end of my journey (hopefully never!), I want this blog to be **a form of reflection** for myself (and maybe others) in the years to come. 

In addition, I want to have fun with my blog. This blog is meant to be a part of who I am and what are important to my life at the moment, as I have mentioned before. Which brings me to talk about the next topic....

###My Direction in Blogging
Every time I write something on this blog, I want it to document a part of my life. So as I have mentioned in the beginning, the posts in this blog will be relevant to events and situations that are happening to me in real life, around the time I post up my posts. 

And it just so happen, as you will read, I am currently battling an addiction to milk tea. It is a huge part of my life -- I probably averaged a cup a day since the summer before my freshman year of college (Summer 2013) up until the beginning of this summer (Summer 2015). That's a solid *two years*. So definitely expect a lot of posts about milk tea and my expertise on this subject!

Though being addicted to milk tea *isn't the only thing* I have going on in my life. I am currently trying to figure out my goals in college and in life. So by writing about them and my attempts to achieve these goals, I will have a reminder of the success and challenges I've faced. And in my perspective, these will remind me of my motivation when I fall. And hopefully, people (specifically incoming college freshmen and high school students -- more on this later) will be able to use my experiences as a guide.

And honestly, I love to talk so this is for my friends who are just *sooooooo sick* of me opening my mouth. Just kidding, I love every single one of them. They are the reasons I am the person who I am.   

###My Target Audience (SINGLE)
Her name is Isabella Marie Swan. She is a senior in high school, and an incoming Freshman in Dartmouth. At the moment, she is blind to what her life will when she leaves Forks, Washington for college. And to be honest, she has a bit of an addiction or obsession. 

As a hobby, she spends a lot of time reading. Isabella tends to keep to herself at most times but enjoys looking up materials she wishes to know more about. I guess you can call her *Curious <strike>George</strike> Isabella*. It has actually have gotten her into a lot of trouble in the past, including breaking her heart. 

*Props to you if you understood this reference. If you didn't, I'll credit it at the end of this post.*

I pretty much copied this out of my notebook. Not word for word but you get the point. This is me using what I've learned this summer to create content with someone specific in mind. And in doing so, hopefully I can make my content more focused. Though I wouldn't be surprise if my friends (who honestly don't fit the target audience description whatsoever) read my posts and then bring it up to me in person to watch me cringe at my own writing. 

###Conclusion
I want to write and I want to inspire. And if you happened to think this post was way too long for you to endure. Here is my response: same. So here is my tl;dr (too long; didn't read) summary: 

1. This blog is a reflection
2. I will be talking about whatever is happening in my life. So at the moment, these things are: milk tea, and college career choices
3. I want high schoolers and incoming college students to read my blog

And in case you are seriously wondering the reference, the reference is to [Twilight](https://en.wikipedia.org/wiki/Twilight_(series)). Specifically the main character in her point of view during *New Moon*. But this doesn't mean, my possible audience is *only* Bella. It's supposed to be a joke -- no insults, I swear. But I hope you can see the key characteristics I'm trying to go off of and ignore the silly ones!
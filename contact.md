---
layout: page
title: Contact Me
permalink: /contact/
---

I love all emails -- whether it is to improve on my website, to tell me how shitty my writing is (trust me, I hear this often) or to connect with me. I have a weird obsession with emails and the need to reply to *everything*, so I will probably reply to every single one. Don't feel shy, mail away!! 

(*A really random thought but related to this somewhat... I think I have too many email addresses. Help!?*)

Of course, if you just have a short blurb/comment, feel free to tweet at me (**@sinnto**). Then we can lose all the formality (if there's ever formality when it comes to me) and get straight to the point. 

*Looking for a topic to talk to me about?* Milk tea is always a great topic to talk to me about. I can literally (and will) talk for hours about milk tea. Or [League of Legends](http://pvp.net). Or [Naruto](https://en.wikipedia.org/wiki/Naruto). Or fashion (or lack of). Or business. 

<br>

####Email Addresses
<br>

*For professional inquiries & blog related things:* [nguyetduong.minh@gmail.com](mailto:nguyetduong.minh@gmail.com)

*For Girls Who Code:* [nguyet.duong@girlswhocode.com](mailto:nguyet.duong@girlswhocode.com)
---
layout: page
title: About Me
image: /assets/media/about/about_me_img_2.jpg
permalink: /about/
---

<img src="{{page.image}}" width="40%" style="float:left; padding-right: 20px"/>

Hi! I'm Mimi (or Nguyet), *the* milk tea connoisseur ([explanation](https://en.wikipedia.org/wiki/Bubble_tea)). I live in California, and spend most of my time on the computer or at a restaurant. I'd like to say that I am pretty young, as I am still getting my undergraduate degree at the **University of California, Berkeley**. 

While it doesn't seem like it, I have many interests in various fields. I specialize in playing [League of Legends](http://pvp.net). In fact, I am currently the official club's president at UC Berkeley. My favorite map is Summoner's Rift; I have over 2000 games on it. I'm just *a little bit* obsessed.

But even though League of Legends is currently my favorite game, [Maplestory](http://maplestory.nexon.net) and [Gaiaonline](http://gaiaonline.com) will always hold a special place in my heart because they both ignited my love for design and programming. Maplestory taught me how to create aesthetically pleasing videos, and Gaiaonline taught me how to use CSS to create an awesome profile (which eventually turned into my profession on the forum). 

However when I'm not designing a new theme for my blog, eating out, drinking milk tea, and sleeping, I'll be watching TV shows. Some various TV shows I watch are [OITNB](https://en.wikipedia.org/wiki/Orange_Is_the_New_Black), [The Big Bang Theory](https://en.wikipedia.org/wiki/The_Big_Bang_Theory), and [Game of Thrones](https://en.wikipedia.org/wiki/Game_of_Thrones). Luckily, I am all caught up with the season so I have time in the fall semester to focus on my academics. Except on weekends. On weekends, I'll be watching [Naruto](https://en.wikipedia.org/wiki/Naruto).

Yes, I have finished reading the manga. I have been following this series since I was in middle school. It is a huge part of my childhood, so everyone should go and read/watch it! 

Oh, fair warning, I used to be obsessed with [Twilight](https://en.wikipedia.org/wiki/Twilight_(series)). I probably just lost all of my readers....

[Find out more about what my blog is about.](/site-management/2015/07/31/this-blog-and-me.html)